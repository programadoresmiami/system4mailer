<?php
namespace controller;

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use conexion;

require('settings.php');
require('model/conexion.php');
require('vendor/autoload.php');

class System4Mailer
{
  private $id; //id
  private $email; //email;
  private $name; //full_name;
  private $status; //status
  private $open; //open

  const TB1 = 'contactos_emails';

  function __construct() {
      $this->id;
      $this->email;
      $this->name;
      $this->status;
      $this->open;
  }
  public function getLastTenUsers(){
    $con = new conexion();
    $consulta = $con->prepare("SELECT c.*  FROM contactos c LEFT JOIN contactos_emails ce ON ce.contacto_id=c.id WHERE ISNULL(ce.id) LIMIT 3");
    $reg = $consulta->execute() ? $consulta->fetchAll(): false;
    $con = null;
    return $reg;
    
  }
  public function clienteOpenEmail($id){
    $con = new conexion();
    $consulta = $con->prepare("UPDATE ".self::TB1." SET status = 2, fecha_abierto = CURRENT_TIMESTAMP WHERE id = ".$id);
    $reg = $consulta->execute() ? true: false;
    $con = null;
    return $reg;
  }
  public function updateStatus($contacto_id, $status){
    $con = new conexion();
    $consulta = $con->prepare("INSERT INTO ".self::TB1." (contacto_id, fecha, hora, status) VALUES ($contacto_id, CURRENT_DATE, CURRENT_TIME, 1)");
    $reg = $consulta->execute() ? true: false;
    $con = null;
    return $reg;
  }
  public function sendEmail($email, $subject, $mensaje, $contacto_id){
    $mail = new PHPMailer(true);
    $track_code = "<img border='0' src='https://topturfmiami.system4book.com/system4mailer?open=" . $contacto_id . "' style='opacity: 0;'/>";
    try {
        //Server settings
        $mail->SMTPDebug = DEBUG; // Enable verbose debug output
        $mail->isSMTP(); // Set mailer to use SMTP
        // $mail->isMail(); // Set mailer to use Mail
        $mail->Host       = STMP_HOST;  // Specify main and backup SMTP servers
        $mail->SMTPAuth   = true; // Enable SMTP authentication
        $mail->Username   = USER; // SMTP username
        $mail->Password   = PASSWORD; // SMTP password
        // $mail->SMTPSecure = ENCRYPTION; // Enable TLS encryption, `ssl` also accepted
        $mail->Port       = PORT; // TCP port to connect to

        $mail->SMTPOptions = array(
          'ssl' => array(
            'verify_peer' => false,
            'verify_peer_name' => false,
            'allow_self_signed' => true
          )
        );

        //Recipients
        $mail->setFrom(USER, 'TopTurfArtificialGrass.com');
        $mail->addAddress($email); 
        // Content
        $mail->isHTML(true); // Set email format to HTML
        $mail->Subject = $subject;
        $mail->Body    = $mensaje . $track_code;
        // $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

        $mail->send();
        if($this->updateStatus($contacto_id, 1)){
          echo "Message has been sent. to: ".$email."\n";
        }
    } catch (Exception $e) {
        echo "\n\n Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
    }
  }
}

?>