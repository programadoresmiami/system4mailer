<?php
include_once('controller/System4Mailer.php');
set_time_limit(0);
use \controller\System4Mailer;
$System4Mailer = new System4Mailer();
if (isset($_GET['open'])) {
  $id= $_GET['open'];
  $System4Mailer->clienteOpenEmail($id);
  exit;
} else {
  $contactos = $System4Mailer->getLastTenUsers();
  foreach ($contactos as $con) {
    $r = rand(5, 10);
    sleep($r);
    $contacto_id = $con['id'];
    $email = $con['email'];
    $fullname = ucwords(strtolower($con['nombre'] ." ". $con['apellido']));
    $subject = 'Notificación Top Turf Artificial Grass - '. $fullname;
    include('email.php');
    try {
      $mail = new System4Mailer();
      $mail->sendEmail($email, $subject, $html, $contacto_id);
      echo "true";
      echo "<br/>";
    } catch (\Throwable $th) {
      echo 'Excepción capturada: ',  $th->getMessage(), "\n";
      echo "<br/>";
    }
  }
}
?>