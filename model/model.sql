CREATE DATABASE `sellamaileranalitics` /*!40100 COLLATE 'utf8_general_ci' */;
USE `sellamaileranalitics`;

CREATE TABLE `users` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`email` VARCHAR(100) NOT NULL,
	`full_name` VARCHAR(50) NULL DEFAULT NULL,
	`last_session` VARCHAR(20) NULL DEFAULT NULL,
	`id_status` INT(11) NOT NULL DEFAULT '1',
	`open` BINARY(1) NOT NULL DEFAULT '0',
	`open_date` TIMESTAMP NULL DEFAULT NULL,
	PRIMARY KEY (`id`),
	UNIQUE INDEX `email` (`email`),
	INDEX `id_status` (`id_status`)
)COLLATE='utf8_general_ci'
ENGINE=InnoDB;
ALTER TABLE `users`	AUTO_INCREMENT=1;

CREATE TABLE `status` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`desciption` VARCHAR(20) NOT NULL,
	PRIMARY KEY (`id`)
)COLLATE='utf8_general_ci'
ENGINE=InnoDB;
  
INSERT INTO `sellamaileranalitics`.`status` (`id`, `desciption`) VALUES (1, 'pending');
INSERT INTO `sellamaileranalitics`.`status` (`id`, `desciption`) VALUES (2, 'sent');

--INSERT USERS EXAMPLE
--INSERT INTO `users` (`id`, `email`, `full_name`, `last_session`, `id_status`, `open`) VALUES (1, 'fernandoaponte90@gmail.com', 'FERNANDO APONTE', NULL, 1, '0');

--UPDATE USERS open to 1
--UPDATE `sellamaileranalitics`.`users` SET `open`=1 WHERE  `id`=1;