<?php
include_once('settings.php');

class conexion extends PDO { 
   private $tipo_de_base = 'mysql';
   private $host = '127.0.0.1';
   // private $puerto = DB_PORT;
   private $nombre_de_base = DB_NAME;
   private $usuario = DB_USER;
   private $contrasena = DB_PASSWORD;

   public function __construct(){   	
      try {
         parent::__construct($this->tipo_de_base.':host='.$this->host.';dbname='.$this->nombre_de_base.';charset=UTF8', $this->usuario, $this->contrasena);
      }catch(PDOException $e){
         echo 'err: ' . $e->getMessage();
         exit;
      }
   } 
 } 

?>