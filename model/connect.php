<?php
function connect(){
	$host = 'host=localhost';
	$port = 'port='. DB_PORT;
	$dbname = 'dbname='. DB_NAME;
	$user = 'user='. DB_USER;
	$password = 'password='. DB_PASSWORD;

	$db = pg_connect("$host $port $dbname $user $password");
	if (!$db) {
		echo 'Error: '.pg_last_error();
	} else {
		return $db;
	}
}
?>