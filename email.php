<?php
if ($con['idioma'] != 'ESP') {
  $t1 = "Querido ";
  $t3 = "Notificación";
  $t2 = "El presente correo electrónico es para informar a nuetra clientela que el señor Eric Infante ya no trabaja más en nuestra empresa, cualquier información sobre estimados, garantias, precios o cualquier información llamar al señor <a href='tel:3059862255' style='color: #4f8e3a; font-weight: 700'>Gabriel Caroso 305-986-2255</a>";
} else {
  $t1 = "Dear ";
  $t3 = "Notification";
  $t2 = "This email is to inform our clients that Mr. Eric Infante no longer works in Top Turf Miami, any information about estimates, prices, guarantees or any other information please call <a href='tel:3059862255' style='color: #4f8e3a; font-weight: 700'>Mr. Gabriel Caroso 305-986-2255</a>";
}

$html = '
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<base target="_blank">
<title></title>
</head>
<body style="margin: 0">
  <table width="100%" bgcolor="#eeeeee" cellpadding="0" cellspacing="0" border="0" style="padding:50px 0; background: -webkit-linear-gradient(-50deg, #82caa5 0%, #58949e 100%);">
    <tbody>
      <tr>
        <td>
          <table cellpadding="0" cellspacing="0" width="600" border="0" align="center" style="border-radius: 2px">
            <tbody>
              <base target="_blank">
              <tr>
                <td colspan="3" rowspan="3" bgcolor="#FFFFFF" style="border-radius: 2px">
                  <table width="100%" bgcolor="#1c363c" style="padding: 15px 30px;border-top-left-radius: 2px;border-top-right-radius: 2px;">
                    <tbody>
                      <tr>
                        <td>
                          <a href=""><img src="http://topturfmiami.com/ref/correo/logo.png" alt=""></a>
                        </td>
                        <td>
                          <h1 style="color: #fff!important;margin: 38px 0 0 0;text-align: right;line-height: 1;font-size: 40px;font-weight: 600;font-family: Arial, sans-serif">'.$t3.'</h1>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                  <table width="100%" style="padding:30px">
                    <tbody>
                      <tr>
                        <td style="text-align:center">
                          <h2 style="color:#333!important;margin: 0;padding: 0 0 30px 0 ;text-align: left;font-size: 35px;font-weight:  100;font-family:  Arial, sans-serif;">'.$t1.$fullname.',</h2>
                          <p style="color:#333!important;font-size: 19px;margin: 0;text-align: justify;font-weight: 400;font-family: Arial, sans-serif;">'.$t2.'</p>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                  <table width="100%" style="padding:30px">
                    <tbody>
                      <tr>
                        <td style="text-align:center">
                          <a style="background: -webkit-linear-gradient(-50deg, #82caa5 0%, #58949e 100%);color: #1c363c!important;margin: 0;padding: 10px 20px;border-radius: 2px;text-decoration:none!important;font-size: 25px;font-weight: 100;letter-spacing: .5px;font-family: Arial, sans-serif;" title="Top Turf Miami Website" href="www.topturfmiami.com">Top Turf Miami</a>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                  <table width="100%" style="padding:30px 30px 0 30px">
                    <tbody>
                      <tr>
                        <td style="text-align:center">
                          <h3 style="color:#333!important;margin: 0;text-align: center;font-size: 30px;font-weight: 400;font-family: Arial, sans-serif;display:  inline-block;float:  left;margin-top: 11px;">Top Turf Miami</h3>
                          <a style="margin: 0 10px;background: -webkit-linear-gradient(-50deg, #82caa5 0%, #58949e 100%);display:  inline-block;float: right;border: 0!important;line-height: 0!important;padding: 10px;border-radius: 50%;" title="Top Turf Miami Pinterest" href="//pinterest.com/topturfmiami/"><img src="http://topturfmiami.com/ref/correo/pinterest.png" alt="Top Turf Miami Pinterest"></a>
                          <a style="margin: 0 10px;background: -webkit-linear-gradient(-50deg, #82caa5 0%, #58949e 100%);display:  inline-block;float: right;border: 0!important;line-height: 0!important;padding: 10px;border-radius: 50%;" title="Top Turf Miami Instagram" href="//instagram.com/topturf/"><img src="http://topturfmiami.com/ref/correo/instagram.png" alt="Top Turf Miami Instagram"></a>
                          <a style="margin: 0 10px;background: -webkit-linear-gradient(-50deg, #82caa5 0%, #58949e 100%);display:  inline-block;float: right;border: 0!important;line-height: 0!important;padding: 10px;border-radius: 50%;" title="Top Turf Miami Facebook" href="//facebook.com/topturfmiami/"><img src="http://topturfmiami.com/ref/correo/facebook.png" alt="Top Turf Miami Facebook"></a>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                  <div style="width:600px; text-align:center">
                    <hr style="border: .5px solid #003b44;margin:5px 30px">
                  </div>
                  <table width="100%" style="padding:0 30px 20px 30px">
                    <tbody>
                      <tr>
                        <td style="text-align:center">
                          <address style="font-style: inherit">
                            <a style="color:#333!important;margin: 0;text-decoration:none!important;text-align: center;font-size: 34px;font-weight: 600;font-family: Arial, sans-serif;display: inline-block;float: left;" title="Top Turf Miami Phone" href="tel:8443987110">844-398-7110</a>
                            <a style="color:#333!important;margin: 0;text-decoration:none!important;text-align: center;font-size: 16px;font-weight: 600;font-family: Arial, sans-serif;display: inline-block;margin-top: 15px;float: right;" title="Top Turf Miami Address" href="#">8880 NW 24th Terrace, Doral, FL 33172</a>
                          </address>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
            </tbody>
          </table>
          <table cellpadding="0" cellspacing="0" width="600" border="0" align="center" style="border-radius: 2px">
            <tbody>
              <tr>
                <td>
                  <img src="http://topturfmiami.com/ref/correo/sombra.png">
                </td>
              </tr>
            </tbody>
          </table>
        </td>
      </tr>
    </tbody>
  </table>
</body>
</html>';

// echo $html;